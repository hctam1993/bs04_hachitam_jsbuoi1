/**
 * Input: nhập vào số có 2 chữ số n = 56
 *
 * ToDo:
 *  soHangDonVi = n % 10
 *  soHangChuc = n / 10
 *
 *  tong = soHangDonVi + soHangChuc
 *
 * Output result = tong
 */

var n = 56;

var soHangDonVi = n % 10;
var soHangChuc = Math.floor(n / 10);

var tong = soHangChuc + soHangDonVi;

console.log("tong: ", tong);
