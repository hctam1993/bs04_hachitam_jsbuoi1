/**
 * Input: nhập vào chieuDai vs chieuRong
 *
 * ToDo:
 *  chuVi = (chieuDai + chieuRong) * 2
 *  dienTich = chieuDai * chieuRong
 *
 * Output:
 *  chuVi , DienTich
 */

var chieuDai = 4;
var chieuRong = 5;

var dienTich = chieuDai * chieuRong;
var chuVi = (chieuDai + chieuRong) * 2;

console.log("dienTich: ", dienTich);
console.log("chuVi: ", chuVi);
