/**
 * Input: nhập vào 5 số thực num1,num2,num3,num4,num5
 *
 * ToDo:
 *  average = (num1 + num2 + num3 + num4 + num5)/5;
 *
 * Output result = average
 */
var num1 = 1;
var num2 = 2;
var num3 = 3;
var num4 = 4;
var num5 = 5;

var average = (num1 + num2 + num3 + num4 + num5) / 5;

console.log("average: ", average);
